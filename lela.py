#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import readchar                 # install with pip if not already installed
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
from blessings import Terminal   # install with pip if not already installed

# user variables...
data='data.csv'
rules='rules.csv'
repeat_delay=4
relearn_delay=8
# other
sandbox={}

with open(data) as csvfile:
    reader = csv.reader(csvfile)
    translations = dict((rows[0],rows[1]) for rows in reader)
    csvfile.seek(0)
    genders = dict((rows[0],rows[2]) for rows in reader)
    csvfile.seek(0)
    applied_rules = dict((rows[0],rows[3]) for rows in reader)
    csvfile.seek(0)
    difficulty = dict((rows[0],int(rows[4])) for rows in reader)

with open(rules) as csvfile:
    reader = csv.reader(csvfile)
    rules = dict((rows[0], rows[1]) for rows in reader)

# wrap in functions to make it easer to move to database

def translation(key):
    global translations
    return(translations[key])

def gender(key):
    global genders
    return(genders[key])

def gender_terminal(mf):          # print string in right colour
    if ( mf == 'f') :
        return term.red
    else:
        return term.blue


def rule(key):
    global rules
    global applied_rules
    rule = applied_rules[key]
    return(rules[rule])


# main section
relearn_delay+=1
repeat_delay+=1
last=""
w=""
asked=0
correct=0
errors={}
term = Terminal()
print term.clear() # clear screen
print "--=Enter", term.bold('m'), "or", term.bold('f'), "for gender or", term.bold('q'), "to quit=--\n\n\n"


while 1:
    asked+=1
    for card in sandbox:
        sandbox[card]['timeout']-=1
        if (sandbox[card]['timeout'] == 0): # timed_out
            difficulty[card]=sandbox[card]['difficulty'] # return to practice

    # for i in xrange(4):
    #     print term.move_down
            
    cards=(iter(sorted(difficulty, key=difficulty.get, reverse=True)))
    while (last == w):          # prevent repeating
        w=next(cards)
    for i in xrange(10):
        print term.move_up
    print term.bold("[" + str(correct) + "/" + str(asked) + "]") + " >>" + w

    while 1:
        mf = readchar.readchar()
        if (mf not in ('m', 'f', 'q')) :
            print "illegal input"
        else :
            break
    for i in xrange(4):
        print term.move_up + term.clear_eol + term.move_up
    if (mf  == 'q'):
        break
    elif (mf == gender(w)):
        print "Congratulations!"
        difficulty[w]-=1
        correct+=1

        if (w in sandbox):
            sandbox[w]['failed']-=1
            if (sandbox[w]['failed'] == 0):
                sandbox.pop(w)
    else:
        print "!!!! Incorrect !!!!"
        difficulty[w]+=1
        if (rule(w) not in errors):
            errors[rule(w)]=0
        errors[rule(w)]+=1
        # sandbox this card for time_out runs
        if (w in sandbox):      # allow 'learning' mode
            n= sandbox[w]['failed'] + 1
            time_out=relearn_delay
            difficulty[w]+=1
        else:
            n=1
            time_out=repeat_delay
        sandbox[w]={'difficulty':difficulty.pop(w),'timeout':time_out,'failed':n}

    print (gender_terminal(gender(w)) + (translation(w) + " (" + gender(w)) + ")" )
    print ("\t Rule: " + rule(w) + term.normal)
    last=w

# exit code
for i in list(sandbox):         # don't iterate whilst changing dict!
    difficulty[i]=sandbox[i]['difficulty']
    sandbox.pop(i)
print "Error statistics:"
for e in errors:
    print e, ":", str(errors[e])
    
# write difficulties back to file
#with open('data2.csv', 'wb') as csvfile:
with open(data, 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    for i in sorted(difficulty):
        writer.writerow([i,translation(i),gender(i),applied_rules[i],difficulty[i]])


print "goodbye"
